<?php

require __DIR__ . '/../vendor/autoload.php';
$router = new \Bramus\Router\Router();

$router->setNamespace('\Http');



$router->get('/', function () {
    readfile('home.html');
});

$router->mount('/api', function () use ($router) {
    $router->get('/tasks', 'TaskController@overview');
    $router->post('/tasks', 'TaskController@add');
    $router->get('/tasks/(\d+)', 'TaskController@methodNotAllowed');
    $router->put('/tasks/(\d+)', 'TaskController@edit');
    $router->patch('/tasks/(\d+)', 'TaskController@methodNotAllowed');
    $router->delete('/tasks/(\d+)', 'TaskController@delete');
});


$router->run();