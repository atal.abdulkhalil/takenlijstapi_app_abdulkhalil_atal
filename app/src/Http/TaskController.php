<?php


namespace Http;


use Services\DatabaseConnector;
use DateTime;

class TaskController extends ApiBaseController
{

    protected \Doctrine\DBAL\Connection $db;

    /**
     * TaskController constructor.
     */
    public function __construct()
    {
        parent::__construct();


        $this->db = DatabaseConnector::getDBConnection();
    }

    public function overview() {
        $tasks = $this->db->fetchAllAssociative('SELECT * FROM tasks ORDER BY priority', []);
        echo json_encode(['tasks' => $tasks]);
    }

    public function delete($id)
    {

            $stmt = $this->db->prepare('DELETE FROM tasks WHERE id = ?');
            $stmt->execute([$id]);
            if ($stmt->rowCount() === 0) {
                $this->message(404, 'task not found');
            }else {
                $this->message(204, 'task has been deleted');
            }
    }


    public function add()
    {
        $bodyParam = $this->httpBody;
        $name = $bodyParam['name'] ?? false;
        $priority = $bodyParam['priority'] ?? false;



        if ($name !== false && $priority !== false) {
            $errors = [];

            if (strlen($name) < 2){
                $errors [] = 'naam moet lanher zijn dan 2 karakters';
            }
            if (! in_array($priority, ['low', 'normal', 'high'])) {
                $errors [] = 'incorecte prioriteit opgegeven';
            }

            if (! $errors) {
                $stmt = $this->db->prepare('INSERT INTO tasks (name, priority, added_on) VALUES (?, ?, ?)');
                $stmt->execute([$name, $priority, (new DateTime())->format('Y-m-d H:i:s')]);

                if ($stmt->rowCount() > 0) {
                    $this->message(201, 'task created');
                }else{
                    $this->message(503, 'unable to create task');
                }
            } else {
                $this->message(422, implode('  ', $errors));
            }
        }else{
            $this->message(400, 'unable to create task');
        }



    }

    public function edit($id)
    {
        $bodyParam = $this->httpBody;
        $name = $bodyParam['name'] ?? false;
        $priority = $bodyParam['priority'] ?? false;

        $stmtControle = $this->db->prepare('SELECT * FROM tasks WHERE id = ?');
        $stmtControle->execute([$id]);

        if ($stmtControle->rowCount() < 1 ) {
            $this->message(404, 'Not foud');
        }

        else{
            if ($name !== false && $priority !== false) {
                $errors = [];

                if (strlen($name) < 2){
                    $errors [] = 'naam moet lanher zijn dan 2 karakters';
                }
                if (! in_array($priority, ['low', 'normal', 'high'])) {
                    $errors [] = 'incorecte prioriteit opgegeven';
                }

                if (! $errors) {
                    $stmt = $this->db->prepare('UPDATE tasks SET name = ?, priority = ? WHERE id = ?');
                    $stmt->execute([$name, $priority, $id]);

                    if ($stmt->rowCount() > 0) {
                        $this->message(200, 'task edited');
                    }else{
                        $this->message(503, 'unable to create');
                    }
                } else {
                    $this->message(422, implode(' ', $errors));
                }
            }else{
                $this->message(400, 'unable to create task');
            }
        }


    }

/*
    public function overview()
    {
        $formErrors = isset($_SESSION['flash']['formErrors']) ? $_SESSION['flash']['formErrors'] : [];
        $what = isset($_SESSION['flash']['what']) ? $_SESSION['flash']['what'] : '';
        $priority = isset($_SESSION['flash']['priority']) ? $_SESSION['flash']['priority'] : 'low';
        unset($_SESSION['flash']);

        $tasksRow = $this->db->fetchAllAssociative('select * From tasks WHERE user_id = ? order by priority', [$this->user['id']]);

        foreach ($tasksRow as $taskRow) {
            $tasks[] = Task::fromArray($taskRow);
        }

        $mailer = new EmailService();
        $mailer->test($this->user['email'], $this->user['username'], $tasks);

        echo $this->twig->render('pages/home.twig', [
            'what' => $what,
            'priority' => $priority,
            'formErrors' => $formErrors,
            'tasks' => $tasks,
            'authenticated' => true
            ]);
    }


    public function create()
    {
        $priority = isset($_POST['priority']) ? trim($_POST['priority']) : '';
        $what = isset($_POST['what']) ? trim($_POST['what']) : '';

        if (isset($_POST['moduleAction']) && ($_POST['moduleAction'] == 'add')) {

            if (strlen($what) < 3 ){
                $formErrors[] = 'te weinig karakters';
            }

            if (! in_array($priority, ['low', 'normal', 'high'])) {
                $formErrors[] = 'Incorecte perioriteit opgegeven';
            }

            if (! $formErrors) {
                $stmt = $this->db->prepare('INSERT INTO tasks (name, priority, added_on, user_id) VALUES (?, ?, ?, ?)');
                $stmt->execute([$what, $priority, (new DateTime())->format('Y-m-d H:i:s'), $this->user['id']]);
            } else {
                $_SESSION['flash'] = ['formErrors' => $formErrors,
                    'what' => $what,
                    'priority' => $priority];
            }
        }
        header('Location: /tasks' );
        exit();
    }


    public function showEdit(string $id)
    {
        $formErrors = isset($_SESSION['flash']['formErrors']) ? $_SESSION['flash']['formErrors'] : [];
        $task = isset($_SESSION['flash']['task']) ? $_SESSION['flash']['task'] : null;
        unset($_SESSION['flash']);

        $taskRow = $this->db->fetchAssociative('select * From tasks WHERE id = ? AND user_id = ?', [$id, $this->user['id']]);

        if ($taskRow === false){
            header('Location: /tasks');
            exit();
        }

        if ($task === null) {
            $task = Task::fromArray($taskRow);
        }


        echo $this->twig->render('pages/task-edit.twig', [
            'task' => $task,
            'formErrors' => $formErrors,
            'authenticated' => true
        ]);
    }


    public function edit()
    {
        $priority = isset($_POST['priority']) ? trim($_POST['priority']) : '';
        $what = isset($_POST['what']) ? trim($_POST['what']) : '';
        $id = isset($_POST['id']) ? trim($_POST['id']) : 0;

        if (isset($_POST['moduleAction']) && ($_POST['moduleAction'] == 'edit')) {

            if (strlen($what) < 3 ){
                $formErrors[] = 'te weinig karakters';
            }

            if (! in_array($priority, ['low', 'normal', 'high'])) {
                $formErrors[] = 'Incorecte perioriteit opgegeven';
            }

            if (! $formErrors) {
                $stmt = $this->db->prepare('UPDATE tasks Set name = ?, priority = ? WHERE id = ? AND user_id = ?');
                $stmt->execute([$what, $priority, $id, $this->user['id']]);
                header('Location: /tasks');
                exit();
            }
        }

        $_SESSION['flash'] = ['formErrors' => $formErrors,
            'task' => new Task($id, $what, $priority, new DateTime())];
        header('Location: /tasks/' . $id . '/edit' );
        exit();
    }


    public function showDelete($id)
    {
        $taskRow = $this->db->fetchAssociative('select * From tasks WHERE id = ?', [$id]);

        if ($taskRow === false){
            header('Location: /taskRow');
            exit();
        }

        $task = Task::fromArray($taskRow);
        echo $this->twig->render('pages/task-delete.twig', [
            'task' => $task,
            'authenticated' => true
        ]);
    }


    public function delete($id)
    {
        if (isset($_POST['moduleAction']) && ($_POST['moduleAction'] == 'delete')) {
                $stmt = $this->db->prepare('DELETE FROM tasks WHERE id = ?');
                $stmt->execute([$id]);
                header('Location: /tasks');
                exit();

        }
    }
*/

}